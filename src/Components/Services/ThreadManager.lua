------  INITIALIZATION
----  variable localization
local component = component
local PBCore = PBCore
local Utilities = PBCore:depend("Utilities")
local merge = Utilities.merge
local assign = Utilities.assign
local Enums = PBCore:depend("EnumsManager").Enums
local newObject = PBCore.newObject
local newObjectClass = PBCore.newObjectClass
local lookupAny = objectData.lookupAny
local coroutine = coroutine
local resumeThread = coroutine.resume
local yieldRunningThread = coroutine.yield
local getThreadStatus = coroutine.status
local createThread = coroutine.create
local getRunningThread = coroutine.running
local require = require
local setmetatable = setmetatable
local os = os
local getTime = os.time
local table = table 
local sortTable = table.sort
local remove = table.remove
local sleep = require("timer").sleep
local __private_Component = lookupAny(component)
local lookupThreadToSession = lookupAny(PBCore).lookupThreadToSession
------  DECLARATION
----  variables
local totalTasks = setmetatable(
    {},
    {
        __mode = "v"
    }
) __private_Component.totalTasks = totalTasks
local lookupTaskByThread = setmetatable(
    {},
    {
        __mode = "kv"
    }
) __private_Component.lookupTaskByThread = lookupTaskByThread
----  classes
--  @classdef  Task
--  @desc  Represents a scheduled task within PigBot
--  @permissions:
--  read:    2 (SERVICE)
--  write:   4 (NONWRITABLE)
--  execute: 2 (SERVICE)
newObject(
    "Enum",
    "TaskPriority",
    {
        Normal = 1,
        Service = 2,
        System = 3
    }
)
local classdefTask = newObjectClass(
    "Task", 
    lookupAny(Object).class
) do
    local __private_Task = lookupAny(Task)
    ----  OnCreate
    function __private_Task:onCreate(timeSuspended, priority, thread, arguments)
        lookupAny(Object).onCreate(self)
        if (timeSuspended == nil) then
            timeSuspended = 0
        end
        if (priority == nil) then
            priority = Enums.TaskPriority.Normal
        end
        if (thread == nil) then 
            thread = getRunningThread()
        end
        ----  declaration
        local __private_self = lookupAny(self)
        assign(
            __private_self,
            {
                timeSuspended = timeSuspended + getTime(),  --  How shall we suspend this task until we resume it?
                priority = priority,  --  Priority of the task
                thread = thread,  --  Reference to the actual thread itself
                sidelined = 0,  --  how many times has this task been sidelined by a higher priority task?
                created = getTime(),  --  when was the thread created?
                runTime = 0,  --  how many seconds has this thread actually used the CPU?
                arguments = arguments
            }
        )
        lookupTaskByThread[thread] = self
    end
    ----  CLASS API
    --  @param  none
    --  @desc  Returns the internal information within a proxy table
    --  @returns  table
    function classdefTask:getDetails()
        local __private_self = lookupAny(self)
        return merge(
            {
                timeSuspended = __private_self.timeSuspended,
                priority = __private_self.priority,
                thread = __private_self.thread,
                sidelined = __private_self.sidelined,
                created = __private_self.created,
                runTime = __private_self.runTime,
                arguments = __private_self.arguments
            }
        ), __private_self
    end
end
--  @classdef  Scheduler
--  @desc  Abstract singleton which operates the PigBot Scheduler.
--  @permissions:
--  read:       2 (SERVICE)
--  write:      4 (NONWRITABLE)
--  execute:    2 (SERVICE)
local classdefScheduler = newObjectClass(
    "Scheduler", 
    lookupAny(Object).class
) do
    local __private_Scheduler = lookupAny(Scheduler)
    local scheduled = setmetatable(
        {},
        {
            --__mode = "v"
        }
    ) __private_Component.scheduled = scheduled
    ----  OnCreate
    function __private_Scheduler:onCreate()
        return error(Enums.Fatal.ClassNotCreatable:format("Scheduler"))
    end
    ----  Scheduler API
    --  @param  number timeSuspended,  coroutine thread
    --  @desc  Schedules the thread for timeSuspended
    --  @returns  none
    local function sortScheduler(task1, task2)
        local task1Info = task1:getDetails()
        local task2Info = task2:getDetails()
        if (task1Info.sidelined > 5) then
            --  override priority as this task has been sidelined too many times
            task1Info.sidelined = 0
            return true 
        end
        if (task1Info.priority > task2Info.priority) then
            --  otherwise, enforce task priority
            task2Priority.sidelined = task2Priority.sidelined + 1
            return true
        end
        return false
    end
    --  @param  number timeSuspended,  coroutine  thread,  vararg ...
    --  @desc  Schedules the thread to be resumed at a later time, passing optional arguments along
    --  @returns  Task1
    function classdefScheduler:schedule(timeSuspended, thread, ...)
        if timeSuspended == nil then
            timeSuspended = 0
        end
        if thread == nil then
            thread = getRunningThread()
        end
        local requestingSession = lookupThreadToSession[thread]
        local arguments = {...}
        --  check to see if we already have an existing task
        local task, __private_Task = lookupTaskByThread[thread]
        if (task == nil) then
            --  the task has not been previously created.
            task = newObject(
                "Task",
                nil,
                timeSuspended,
                Enums.TaskPriority.Normal,
                thread,
                arguments
            )
            __private_Task = lookupAny(task)
        else
            __private_Task = lookupAny(task)
            __private_Task.suspendedTime = timeSuspended + getTime()
            __private_Task.arguments = arguments
        end 
        ----  sort and prioritize
        if requestingSession:getComponent():isA("Service") then
            taskPrivate.priority = Enums.TaskPriority.Service
        elseif Construct.__private.loadedObjects[requestingSession:getComponent()].objectId == 1 then
            taskPrivate.priority = Enums.TaskPriority.System
        end
        --  sort
        scheduled[#scheduled + 1] = task
        sortTable(
            scheduled,
            sortScheduler
        )   
        return task
    end
    --  @param  number timeSuspended
    --  @desc  Suspends the running thread for timeSuspended seconds
    --  @returns  none
    function classdefScheduler.wait(timeSuspended, ...)
        classdefScheduler:schedule(
            timeSuspended,
            getRunningThread(),
            ...
        )
        return yieldRunningThread()
    end
    --  @param  function callback
    --  @desc  Execute the callback as a separate thread immediately after the calling thread is finished
    --  @returns  Object
    function classdefScheduler.spawn(callback, ...)
        return classdefScheduler:schedule(
            0,
            createThread(callback),
            ...
        )
    end
    --  @param  number timeSuspended,  function callback
    --  @desc  Creates a task and schedules it to execute within timeSuspended seconds
    --  @returns  Object
    function classdefScheduler.delay(timeSuspended, callback)
        return classdefScheduler:schedule(
            timeSuspended or 0,
            createThread(callback)
        )
    end
    --  @param  
end 
root.Scheduler = classdefScheduler
--  @param  none
--  @desc  Toggles the activity of the Scheduler
--  @returns  booelean
local schedulerActive = true
local schedulerThread
function root:toggleScheduler()
    schedulerActive = not schedulerActive
    if schedulerActive == true then
        resumeThread(schedulerThread)
    end
    return schedulerActive
end
------  INITIALIZE
ready()
local selectedTask, taskPrivate, now, activeFor, ran, failure
local scheduled = __private_Component.scheduled
schedulerThread = createThread(
    function()
        while true do
            if schedulerActive == false then yieldRunningThread() end
            now = getTime()
            if (#scheduled > 0) and (__private_Component.scheduled[1]:getDetails().timeSuspended <= now) then
                selectedTask = remove(
                    __private_Component.scheduled,
                    1
                ):getDetails()
                activeFor = getTime()
                ran, failure = resumeThread(
                    selectedTask.thread,
                    unpack(selectedTask.arguments)
                )
                if ran == false then
                    --  collect debug data
                    local session = lookupThreadToSession[selectedTask.thread]
                    local __private_Component = lookupAny(session).ComponentPrivate
                    if selectedTask.Priority > 1 then
                        error(Enums.Failure.SchedulerTaskFailed:format(__private_Component.objectRoot.name, failure))
                    end
                    warn(Enums.Failure.SchedulerTaskFailed:format(__private_Component.objectRoot.name, failure))
                end
                --  track task run time
                __private_Task.runTime = __private_Task.runTime + (getTime() - activeFor)
            end
            sleep(100)
        end
    end
)
resumeThread(schedulerThread)