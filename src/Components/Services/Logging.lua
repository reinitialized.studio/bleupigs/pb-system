------  INITIALIZATION
----  variable declaration
local PBCore = PBCore
local component = component
local newObject = PBCore.newObject
local newObjectClass = PBCore.newObjectClass
local lookupAny = PBCore.lookupAny
local EnumsManager = PBCore:depend("EnumsManager")
local Enums = EnumsManager.Enums
local Utilities = PBCore:depend("Utilities")
local JSON = Utilities.JSON
local FS = Utilities.FileSystem
local assign = Utilities.assign
local merge = Utilities.merge
local splitString = Utilities.splitString
local generateTimestamp = Utilities.generateTimestamp
PBCore:depend("SignalsManager")
local Scheduler = PBCore:depend("ThreadManager").Scheduler
local wait = Scheduler.wait
local delay = Scheduler.delay
local spawn = Scheduler.spawn
local os = os
local getTime = os.time
local string = string
local formatString = string.format
local debug = debug
local getStackInfo = debug.getinfo
local getTraceback = debug.traceback
local cachedCriticalLogs = {}
------  Declaration
----  Enums
--  LogLevel
--  @desc  All available LogLevels
newObject(
    "Enum",
    "EventType",
    {
        Debug = 1,
        Info = 2,
        Warn = 3,
        Error = 4, 
        Fatal = 5,
        Security = 6
    }
)
----  Classes
local classdefEntry = newObjectClass('Entry', lookupAny('Object').class) do
    local __private_Entry = lookupAny.Entry
    local entryCreatedController = newObject(
        "LuaSignal", 
        "EntryCreatedSignal"
    )
    root.EntryCreated = entryCreatedController.Signal
    ----  OnCreate
    ----  @param  Enum.EventType EventType, string Message, vararg ...
    function __private_Entry:onCreate(EventType, Message, ...)
        lookupAny('Object').onCreate(self)
        local __private_self = lookupAny(self)
        local arguments = {...}
        for i = 1, #arguments do
            arguments[i] = tostring(arguments[i])
        end
        local messageFormatted = formatString(
            Message, 
            unpack(arguments)
        )
        local createdTimeRaw = getTime()
        local stackInfo = getStackInfo(3)
        local entry = {
            message = messageFormatted,
            messageRaw = Message,
            eventType = EventType,
            created = createdTimeRaw,
            createdTimestamp = generateTimestamp(createdTimeRaw)
            traceback = getTraceback(),
            name = stackInfo.namewhat .. (stackInfo.name ~= nil and .." ".. stackInfo.name),
        }
        __private_Entry.entry = entry
        if entry.eventType > 3 then
            cachedCriticalLogs[#cachedCriticalLogs + 1] = self
        end
        entryCreatedController:fire(self)
    end
    ----  API declaration
    --  @param  none
    --  @desc  Returns the recorded entry
    --  @returns  table
    --  @permissions
    --  read:  2 (SERVICE)
    --  write:  4 (NONWRITABLE)
    --  execute: 2 (SERVICE)
    function classdefEntry:getEn2tryData()
        return lookupAny(self).entry
    end
end
----  API definitions
--  @param  Enum.EventType EventType,  string Message,  vararg ...
--  @desc  Creates a new Entry based off EventType
--  @returns  none
function root.record(EventType, Message, ...)
    newObject(
        "Entry",
        nil,
        EventType,
        Message,
        ...
    )
end
------  FINALIZE
ready()
spawn(
    function()
        local RemoteStorage = PBCore:depend("RemoteStorage")
        -- TODO: implement 
    end
)