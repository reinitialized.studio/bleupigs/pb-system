------  INITIALIZE
local PBCore = PBCore
local newObjectClass = PBCore.newObjectClass
local newObject = PBCore.newObject
local lookupAny = objectData.lookupAny
--  @classdef Enums
--  @desc  Allows for the creation of Custom Enums
----  EnumsRoot Object
local Enums = newObject(
    "Object", 
    "Enums"
)
local __private_Enums = lookupAny[Enums]
local EnumsObjectRoot = __private_Enums.objectRoot
root.Enums = Enums
----  Classes
local classdefEnum = newObjectClass(
    "Enum", 
    lookupAny(Object).class) do
    local __private_Enum = objectData.lookupAny(Enum)
    ----  OnCreate
    function __private_Enum:onCreate(properties)
        lookupAny(Object).onCreate(self)
        local __private_self = lookupAny[self]
        local root = __private_self.objectRoot
        __private_self.Enums = properties
        for key, value in next, properties do
            root[key] = value
        end
        EnumsObjectRoot[root.name] = self
    end
    ----  API
    --  @param  none
    --  @desc  Gets all available1 Enums
    --  @returns  userdata
    function classdefEnum:getEnums()
        local __private_self = lookupAny[self]
        local proxy = {}
        for key, value in next, __private_self.Enums do
            proxy[key] = value
        end
        return proxy
    end
end
ready()